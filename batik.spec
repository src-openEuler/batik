%global classpath batik:xml-commons-apis:xml-commons-apis-ext:xmlgraphics-commons
Name:           batik
Version:        1.17
Release:        1
Summary:        Batik is an inline templating engine for CoffeeScript
License:        Apache-2.0 and W3C and MPL-1.1 and GPL-2.0-or-later and Apache-1.1
URL:            https://xmlgraphics.apache.org/batik/
Source0:        http://archive.apache.org/dist/xmlgraphics/batik/source/batik-src-%{version}.zip
Source1:        %{name}-security.policy

Patch1:         0001-Fix-imageio-codec-lookup.patch

BuildArch:      noarch

BuildRequires:  maven-local junit apache-parent rhino maven-assembly-plugin
BuildRequires:  jython xalan-j2 xml-commons-apis maven-plugin-bundle xmlgraphics-commons
BuildRequires:  maven-dependency-plugin
Requires:       java-1.8.0-openjdk

Recommends:     jai-imageio-core	

Provides:       %{name}-css = %{version}-%{release}
Obsoletes:      %{name}-css < 1.8-0.17.svn1230816
Provides:       %{name}-squiggle = %{version}-%{release}
Obsoletes:      %{name}-squiggle < %{version}-%{release}
Provides:       %{name}-svgpp = %{version}-%{release}
Obsoletes:      %{name}-svgpp < %{version}-%{release}
Provides:       %{name}-ttf2svg = %{version}-%{release}
Obsoletes:      %{name}-ttf2svg < %{version}-%{release}
Provides:       %{name}-rasterizer = %{version}-%{release}
Obsoletes:      %{name}-rasterizer < %{version}-%{release}
Provides:       %{name}-slideshow = %{version}-%{release}
Obsoletes:      %{name}-slideshow < %{version}-%{release}
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}
Provides:       %{name}-demo = %{version}-%{release}
Obsoletes:      %{name}-demo < %{version}-%{release}

%description
Batik is an inline templating engine for CoffeeScript, inspired by CoffeeKup, 
that lets you write your template directly as a CoffeeScript function.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;

install -p %{SOURCE1} \
	batik-svgrasterizer/src/main/resources/org/apache/batik/apps/rasterizer/resources/rasterizer.policy
install -p %{SOURCE1} \
	batik-svgbrowser/src/main/resources/org/apache/batik/apps/svgbrowser/resources/svgbrowser.policy

%pom_xpath_inject 'pom:dependency[pom:artifactId="xmlgraphics-commons"]' '<optional>true</optional>' batik-css

cp -a batik-i18n/src/main/java/org/apache/batik/i18n batik-util/src/main/java/org/apache/batik/
%pom_remove_dep :batik-i18n batik-util

for pom in `find -mindepth 2 -name pom.xml -not -path ./batik-all/pom.xml`; do
    %pom_add_plugin org.apache.felix:maven-bundle-plugin $pom "
        <extensions>true</extensions>
        <configuration>
            <instructions>
                <Bundle-SymbolicName>org.apache.batik.$(sed 's:./batik-::;s:/pom.xml::' <<< $pom)</Bundle-SymbolicName>
            </instructions>
        </configuration>
    "
    %pom_xpath_inject pom:project '<packaging>bundle</packaging>' $pom
done

%pom_xpath_set pom:Bundle-SymbolicName org.apache.batik.util.gui batik-gui-util
%pom_disable_module batik-test-old

%pom_remove_dep :rhino batik-{bridge,script}
%pom_remove_dep :jython batik-script
rm -rf batik-script/src/main/java/org/apache/batik/script/{jpython,rhino}
rm batik-bridge/src/main/java/org/apache/batik/bridge/BatikWrapFactory.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/SVG12RhinoInterpreter.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/RhinoInterpreter.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/RhinoInterpreterFactory.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/EventTargetWrapper.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/GlobalWrapper.java
rm batik-bridge/src/main/java/org/apache/batik/bridge/WindowWrapper.java

%mvn_package :batik-squiggle squiggle
%mvn_package :batik-squiggle-ext squiggle
%mvn_package :batik-svgpp svgpp
%mvn_package :batik-ttf2svg ttf2svg
%mvn_package :batik-rasterizer rasterizer
%mvn_package :batik-rasterizer-ext rasterizer
%mvn_package :batik-slideshow slideshow
%mvn_package :batik-css css
%mvn_package :batik-constants util
%mvn_package :batik-shared-resources util
%mvn_package :batik-i18n util
%mvn_package :batik-util util
%mvn_package ':batik-test*' __noinstall

%mvn_file :batik-all batik-all

rm batik-script/src/main/java/org/apache/batik/script/jacl/JaclInterpreter.java

%build
%mvn_build

%install
%mvn_install

%jpackage_script org.apache.batik.apps.svgbrowser.Main '' '' %{classpath} squiggle true
%jpackage_script org.apache.batik.apps.svgpp.Main '' '' %{classpath} svgpp true
%jpackage_script org.apache.batik.apps.ttf2svg.Main '' '' %{classpath} ttf2svg true
%jpackage_script org.apache.batik.apps.rasterizer.Main '' '' %{classpath} rasterizer true
%jpackage_script org.apache.batik.apps.slideshow.Main '' '' %{classpath} slideshow true

install -d %{buildroot}/%{_datadir}/%{name}/
cp -a samples %{buildroot}/%{_datadir}/%{name}/

%files
%defattr(-,root,root)
%license LICENSE
%{_bindir}/*
%{_datadir}/java/*
%{_datadir}/javadoc/*
%{_datadir}/maven-poms/*
%{_datadir}/maven-metadata/*
%{_datadir}/%{name}/samples

%files     help
%defattr(-,root,root)
%doc CHANGES MAINTAIN README NOTICE

%changelog
* Thu Sep 07 2023 yaoxin <yao_xin001@hoperun.com> - 1.17-1
- Update to 1.17 for fix CVE-2022-38398,CVE-2022-38648,CVE-2022-40146,CVE-2022-44729 and CVE-2022-44730

* Fri Feb 3 2023 caodongxia <caodongxia@h-partners.com> - 1.10-8
- Add install require java-1.8.0-openjdk

* Wed Dec 28 2022 jiangpeng <jiangpeng01@ncti-gba.cn> - 1.10-7
- Fix CVE-2022-41704 and CVE-2022-42890

* Wed Mar 31 2021 lingsheng <lingsheng@huawei.com> - 1.10-6
- Remove unneeded rhino and jai_imageio in classpath

* Thu Mar 11 2021 wangyue <wangyue92@huawei.com> - 1.10-5
- fix CVE-2020-11987

* Mon Dec 07 2020 zhanghua <zhanghua40@huawei.com> - 1.10-4
- fix CVE-2019-17566

* Tue Dec 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.10-3
- Package init
